#!/bin/sh

###############################
#
# Install aclpy python packages
# Mark Cutler -- October 16, 2013
#
# Note: this script is called at the end of install.sh
###############################

echo "Install the python packages"
cd py
python setup.py install
cd ../
