#!/usr/bin/env python
'''
Description: Implements the rmax algorithm from this paper:

Algorithm 1
Reinforcement Learning in Finite MDPs: PAC Analysis
http://research.microsoft.com/pubs/178881/strehl09a.pdf

Created on Aug 26, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''
import numpy as np
import copy

#from aclpy import rmax
import rmax

class RmaxDBN(rmax.Rmax):

    def __init__(self, S, A, U, terminal_states, logFile=None, savedLog=None,
                 maxDelta=0.001,
                 gamma=0.95, mr=5, mt=5,
                 s_prime2sa=None, Rmax=0, rand_state=None):

        # call the base class constructor
        rmax.Rmax.__init__(self, S, A, U, terminal_states, logFile, savedLog,
                           maxDelta, gamma, mr, mt, rand_state)

        # init dbn factors
        self.s_prime2sa = copy.copy(s_prime2sa)
        self.factors = []
        self.actions = []
        for i in xrange(self.numS):
            f = Factor(self.mt)
            f.numDisc = len(self.S[i])
            self.factors.append(f)
        for i in xrange(self.numA):
            f = Factor(self.mt)
            f.numDisc = len(self.A[i])
            self.actions.append(f)

        self.initDBN()
        self.Rmax = Rmax

#-----------------------------------------------------------------------------
# Update our estimated transition dynamics
# Override base class implementation with DBN model
#-----------------------------------------------------------------------------
    def updateTransition(self, sa, s_prime):
        for i, f in enumerate(self.factors):
            f.update(sa, s_prime[i])

#-----------------------------------------------------------------------------
# Quick method for determining if transition is known
# Override base class implementation with DBN model
#-----------------------------------------------------------------------------
    def transitionKnown(self, sa):
        for f in self.factors:
            if not f.isKnown(sa)[0]:  # check if that factor is known
                return False
        return True  # if all factors are known, return True

#-----------------------------------------------------------------------------
# Quick method for determining if transition just became known
# Override base class implementation with DBN model
#-----------------------------------------------------------------------------
    def transitionKnownExact(self, sa):
        for f in self.factors:
            known, n = f.isKnown(sa)
            if not known:
                return False
            if n == self.mt:
                return True
        return False

#-----------------------------------------------------------------------------
# Update Q^
# Override base class implementation with DBN model
#-----------------------------------------------------------------------------
    def updateQ(self, i, maxDelta):

        # first time through, generate list of reachable states
        if i == 0:
            self.sa2s_prime = {}
            for sa_index, n in np.ndenumerate(self.nr):
                nextStates = self.getReachableStates(sa_index)
                if len(nextStates) > 0:
                    self.sa2s_prime[sa_index] = nextStates

        maxQdict = {}  # use the dictionary to store the looked up values of Qmax
        for sa_index, ns_list in self.sa2s_prime.iteritems():
            s_index = sa_index[0:self.numS]
            if s_index in self.terminal_states:
                self.Q[sa_index] = 0  # episodic version -- no reward in goal
            else:
                # calc expectation of Q
                expectQ = 0
                # loop over s'
                for ns in ns_list:
                    # only run the max() operator if we haven't looked at that ns yet
                    # WARNING -- this isn't totally correct.  Occasionally, you
                    # will cache a maxQ value that gets updated this iteration.
                    # However, in practice, it speeds things up significantly
                    # and doesn't seem to noticeably hurt performance.
                    if ns.s in maxQdict:
                        maxQ = maxQdict[ns.s]
                    else:
                        maxQ = self.Q[ns.s].max()
                        maxQdict[ns.s] = maxQ
#                     maxQ2 = self.Q[ns.s].max()
#                     if maxQ != maxQ2:
#                         print 'WARNING --- INVALID VALUE *************************'
                    expectQ += ns.pr * maxQ

                # update Q (line 21 of alg. in paper)
                Q = self.Rhat[sa_index] + self.gamma * expectQ
                if not self.rewardKnown(sa_index):  # if reward function is unknown
                    Q = self.Rmax + self.gamma * expectQ
                    Q = min([Q, self.Q[sa_index]])
                delta = np.abs(self.Q[sa_index] - Q)
                if delta > maxDelta:
                    maxDelta = delta
                # we need to remove the cached maxQ value if we are about to
                # update a value of Q[sa] that was currently one of the maxes
                # for that action
                if s_index in maxQdict and self.Q[sa_index] == maxQdict[s_index] and delta > 0:
                    del maxQdict[s_index]
                self.Q[sa_index] = Q


        return maxDelta

#####################
# NEW Functions -- not in base class
#####################

#-----------------------------------------------------------------------------
# Set up generalized transition dynamics based on a mapping of s_prime to s,a
#-----------------------------------------------------------------------------
    def initDBN(self):

        # loop over the factors
        for i, f in enumerate(self.factors):
            sa = np.flatnonzero(self.s_prime2sa[i, :])
            s = np.flatnonzero(self.s_prime2sa[i, :self.numS])
            a = np.flatnonzero(self.s_prime2sa[i, self.numS:])
            f.parents = sa
            pr_ind = [f.numDisc]
            pr_col_sum = []
            for j in xrange(len(s)):
                pr_ind.append(self.factors[s[j]].numDisc)
                pr_col_sum.append(self.factors[s[j]].numDisc)
            for j in xrange(len(a)):
                pr_ind.append(self.actions[a[j]].numDisc)
                pr_col_sum.append(self.actions[a[j]].numDisc)
            f.pr = np.zeros(pr_ind)
            f.pr_col_sum = np.zeros(pr_col_sum)
            f.neg_prob = -np.ones(f.numDisc)


#-----------------------------------------------------------------------------
# Get a list of reachable next states for each a SA pair based on the current
# transition model.
#-----------------------------------------------------------------------------
    def getReachableStates(self, sa):

        # TODO: check if calling self.factorsKnown first will speed things up

        nextStates = []
        for f in self.factors:
            nextStates2 = []
            fprobs = f.getProb(sa)
            for v in xrange(f.numDisc):
                if fprobs[v] > 0.0:
                    if len(nextStates) > 0:
                        for i, ns in enumerate(nextStates):
                            tmp = NextState()
                            tmp.s = ns.s + (v,)
                            tmp.pr = ns.pr * fprobs[v]
                            nextStates2.append(tmp)
                    else:
                        tmp = NextState()
                        tmp.s = (v,)
                        tmp.pr = fprobs[v]
                        nextStates2.append(tmp)
                elif fprobs[v] == -1:
                    return []

            if len(nextStates2) > 0:
                nextStates = copy.copy(nextStates2)

        return nextStates

#==============================================================================
# Simple class for holding information about a factor (parents, counts, name,
# etc.).
#==============================================================================
class Factor:
    def __init__(self, m):  # , name):
#         self.name = name  # factor name (string)
        self.numDisc = 0  # number of discretizations of this factor
        self.pr = []  # conditional probability table of factor counts given parents
        self.parents = []  # list of parents of this factor
        self.pr_col_sum = []  # convenience member holding column sums of self.pr
        self.m = m  # number of times a state-action pair needs to be seen
        self.neg_prob = None  # array for holding -1's when can't really calc probs

#-----------------------------------------------------------------------------
# Update the conditional probability table counts with observed state, action,
# and next state values
#-----------------------------------------------------------------------------
    def update(self, sa, s_prime):
        ind_parents = tuple([sa[i] for i in self.parents])
        ind = (s_prime,) + ind_parents
        self.pr[ind] += 1
        self.pr_col_sum[ind_parents] += 1

#-----------------------------------------------------------------------------
# Quick check to determine if all the factors of a state-action pair are known
# If known, returns true and number of times that s-a pair has been seen
#-----------------------------------------------------------------------------
    def isKnown(self, sa, ind=None):
        # check if we've seen enough s,a pairs for this data to be meaningful
        if ind == None:
            ind = tuple([sa[i] for i in self.parents])
        n = self.pr_col_sum[ind]
        if n >= self.m:
            return True, n
        else:
            return False, n

#-----------------------------------------------------------------------------
# Get the probability of all the next factor values
#-----------------------------------------------------------------------------
    def getProb(self, sa):

        ind = tuple([sa[i] for i in self.parents])

        isKnown, n = self.isKnown(sa, tuple(ind))
        if isKnown:

            prob = self.pr[(Ellipsis,) + ind] / n
        else:
            return self.neg_prob

        return prob



#==============================================================================
# Simple class for holding the a reachable state and it's probability
#==============================================================================
class NextState:
    def __init__(self):
        self.s = 0  # tuple of the next state
        self.pr = 0  # probability of reaching that state
