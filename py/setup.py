#!/usr/bin/env python
from distutils.core import setup
setup(name='acl_utils',
      version='0.1',
      description='Common python functions used in the ACL',
      author='Mark Cutler',
      author_email='markjcutler [at] gmail [dot] com',
      packages=['aclpy'],
      )