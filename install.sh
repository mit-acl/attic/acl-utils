#!/bin/bash

###############################
#
# Install ACL_UTILS library
# Mark Cutler -- March 13, 2013
#
# How to use library:
# - add acl_utils to Libraries (-l) under Tool Settings, C++ Linker, Libraries in Eclipse
#
# Also builds a small pure pursuit library (libpure_pursuit) that relies on Point Cloud Library
# being installed on your machine.  If you want this library also installed, all the install.sh
# script with an argument (sudo ./install.sh 1)
#
# Note that in order to use the pure pursuit library you will need to add the following (or
# equivalent) to your CMakeLists.txt file:
# find_package(PCL 1.2 REQUIRED)
# include_directories(${PCL_INCLUDE_DIRS})
# link_directories(${PCL_LIBRARY_DIRS})
# add_definitions(${PCL_DEFINITIONS})
#
# More on this can be found on the PCL website and examples.
#
###############################

# dependencies
# libbluetooth-dev
# libpcl-1.7-all ?
DEPEND="libbluetooth-dev cmake libpcl-1.7-all libboost-program-options-dev doxygen"
PPA1="v-launchpad-jochen-sprickerhof-de/pcl"

echo "First, check for and install any dependencies needed"
# add ppa for libpcl
if ! grep ^ /etc/apt/sources.list /etc/apt/sources.list.d/* | grep $PPA1; then
	sudo add-apt-repository ppa:$PPA1
	sudo apt-get update
else
	echo "Already added ppa:$PPA1"
fi
for pkg in $DEPEND; do
    if dpkg --get-selections | grep -q "^$pkg[[:space:]]*install$" >/dev/null; then
        echo "$pkg is already installed"
    else
        if apt-get -qq install $pkg; then
            echo "Successfully installed $pkg"
        else
            echo "Error installing $pkg"
            exit -1
        fi
    fi
done

echo "Building the ACL library"
mkdir c/build
cd c/build
cmake ../
make all
if [ $? -eq 0 ]; then
    echo OK
else
    echo
    echo
    echo FAIL
    echo "ERROR: the library failed to compile"
    echo
    echo
    exit -1
fi

echo "Copy header and library files into /usr/local"
rm /usr/local/include/acl -r
mkdir /usr/local/include/acl
# recursively copy files with the proper directory structure
rsync -avm --include='*.h' --include='*.hpp' -f 'hide,! */' ../ /usr/local/include/acl/
cp libacl_utils.so /usr/local/lib


echo "Create acl_utils.conf file and update with ldconfig"
rm /etc/ld.so.conf.d/acl_utils.conf
touch /etc/ld.so.conf.d/acl_utils.conf
cat <<EOF >/etc/ld.so.conf.d/acl_utils.conf
/usr/local/lib
EOF
ldconfig

echo "Building the documentation"
cd ../docs
doxygen Doxyfile
cd ../../
if [ $? -eq 0 ]; then
    echo OK
else
    echo
    echo
    echo FAIL
    echo "Warning: the documentation didn't properly build"
    echo
    echo
    exit -1
fi

echo
echo "Installing aclpy python files"
echo
MY_DIR=`dirname $0`
$MY_DIR/pyinstall.sh

echo
echo "Installation finished"
