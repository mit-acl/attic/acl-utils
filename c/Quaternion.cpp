/*
 * Quaternion.cpp
 *
 *  Created on: May 16, 2014
 *      Author: mark
 */

#include "Quaternion.h"

namespace acl
{

Quaternion::Quaternion()
{
    x = y = z = 0.0;
    w = 1.0;
}

Quaternion::Quaternion(double W)
{
    w = W;
    x = 0.0;
    y = 0.0;
    z = 0.0;
}

Quaternion::Quaternion(double W, double X, double Y, double Z)
{
    w = W;
    x = X;
    y = Y;
    z = Z;
}

Quaternion::~Quaternion()
{
    // TODO Auto-generated destructor stub
}

Quaternion Quaternion::conj()
{
    return Quaternion(w, -x, -y, -z);
}

Quaternion Quaternion::operator+(const Quaternion& other)
{
    double wout = w + other.w;
    double xout = x + other.x;
    double yout = y + other.y;
    double zout = z + other.z;

    return Quaternion(wout, xout, yout, zout);
}

Quaternion Quaternion::operator*(const Quaternion& other)
{
    double wout = w * other.w - x * other.x - y * other.y - z * other.z;
    double xout = w * other.x + x * other.w + y * other.z - z * other.y;
    double yout = w * other.y - x * other.z + y * other.w + z * other.x;
    double zout = w * other.z + x * other.y - y * other.x + z * other.w;

    return Quaternion(wout, xout, yout, zout);
}

Quaternion Quaternion::operator*(const double scalar)
{
    double wout = w * scalar;
    double xout = w * scalar;
    double yout = w * scalar;
    double zout = w * scalar;

    return Quaternion(wout, xout, yout, zout);
}

} /* namespace acl */
