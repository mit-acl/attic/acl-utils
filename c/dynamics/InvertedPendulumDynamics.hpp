/*!
 * \file InvertedPendulumDynamics.hpp
 *
 * Simple Inverted Pendulum.  Pendulum has mass m and length l.
 * The pendulum is not must a point mass, but a rod of constant mass.
 *
 *  Created on: May 28, 2013
 *      Author: Mark Cutler
 *	   Contact: markjcutler@gmail.com
 *
 */

#ifndef INVERTEDPENDULUMDYNAMICS_HPP_
#define INVERTEDPENDULUMDYNAMICS_HPP_

// Global includes
#include <valarray>
#include <boost/bind.hpp>
#include <random>

// Local includes
#include "../utils.hpp"

namespace acl
{

const unsigned int STATE_LENGTH = 2;

/// parameter struct
struct sInvPendParam
{
    double J; ///< Pole moment of inertia (kg m^2)
    double m; ///< Total pole mass (kg)
    double l; ///< Pole length (m)
    double l_cg; ///< length to center of gravity of the pole (m)
    double k_fric; ///< Coefficient of friction of pole
    double a; ///< propeller damping coefficient

    double noise_level; ///< Non-dimensional noise coefficient for simulating
                        ///system noise
};

/// state struct
struct sInvPendState
{
    double theta; ///< Pole angle (rad)
    double dtheta; ///< Pole angular rate (rad/s)
};

/**
 *  InvPend dynamics
 */
class InvPendDynamics
{
public:
    InvPendDynamics();
    virtual ~InvPendDynamics();

    void setForce(double F);
    double getForce(void);
    void setParamStruct(struct sInvPendParam param);
    void setInitialState(struct sInvPendState initState);

    struct sInvPendState getState(void);
    struct sInvPendParam getParams(void);

    void integrateStep(double dt);

private:
    std::string name; ///< inverted pendulum name
    struct sInvPendState state; ///< state
    struct sInvPendParam param; ///< param struct

    double force; ///< Propeller force (N)
    double omega; ///< Propeller speed (rad/s)
    double simTime; ///< simulation time

    // initialize random number generator
    std::default_random_engine generator;
    std::normal_distribution<double> ddtheta_noise;

    std::valarray<double> dynamics(double dt, std::valarray<double> s);
};

} /* namespace acl */
#endif /* INVERTEDPENDULUMDYNAMICS_HPP_ */
