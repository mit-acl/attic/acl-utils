/*!
 * \file CarDynamics.cpp
 *
 * @todo Brief file description
 *
 *  Created on: Mar 22, 2013
 *      Author: Mark Cutler
 *     Contact: markjcutler@gmail.com
 *
 */

#include "CarDynamics.hpp"

namespace acl
{

CarDynamics::CarDynamics()
{

    // Initialize all variables
    simTime = 0.0;
    commandWheelSpeed = true;

    // Set default initial parameters
    //  // RC01
    //  param.B = 10.31;    // From initial optimization
    //  param.C = 1.07; // From initial optimization
    //  param.D = 0.340;    // From initial optimization
    //  param.rF = 0.035/2.0;   // measured
    //  param.rR = param.rF;
    //  param.lF = 0.060;   // measured
    //  param.lR = 0.053;   // measured
    //  param.MaxWheelAngle = 15*PI/180.0; // From initial optimization
    //  param.MaxWheelSpeed = 500; // roughly measured
    //  param.MaxTorque = 0.005; // guess
    //  param.m = 0.20516;  // measured
    //  param.Iz = 3.7672e-04;  // measured
    //  param.IwF = 0.00001; // guess
    //  param.IwR = param.IwF;
    //  param.h = 0.033; // roughly measured
    //  param.muW = 0*0.0001;   // guess
    //  param.omegaCL_a = 50*0.08923; // estimates from matlab's sysid toolbox
    // looking at measured data
    //  param.omegaCL_b = 50*0.08618;

//    // RC02
    param.B = 9.5; // From initial optimization
    param.C = 1.1; // From initial optimization
    param.D = 0.62; // From initial optimization
//    param.rF = 0.0606 / 2.0; // measured
//    param.rR = param.rF;
//    param.lF = 0.1; // measured
//    param.lR = 0.107; // measured
//    param.MaxWheelAngle = 20 * PI / 180.0; // From initial optimization
//    param.MaxWheelSpeed = 345; // roughly measured
//    param.MaxTorque = 0.05; // guess
//    param.m = 0.906; // measured
//    param.Iz = 1.0 / 12.0 * param.m
//               * (0.1 * 0.1 + 0.2 * 0.2); // estimated by bar of width 10cm,
//                                          // length 20cm, mass 906 g
//    param.IwF = 0.00001; // guess
//    param.IwR = param.IwF;
//    param.h = 0.02794; // roughly measured
//    param.muW = 0*0.0001; // guess
    param.omegaCLup_a = 50 * 0.089; // initial optimization
    param.omegaCLup_b = 50 * 0.089; // 50*0.086;
    param.omegaCLdown_a = 50 * 0.03; // initial optimization
    param.omegaCLdown_b = 50 * 0.03;

    // RC03
    param.B = 3; // guess
    param.C = 1.5; // guess
    param.D = .2; // guess
    param.rF = 0.0606 / 2.0; // measured
    param.rR = param.rF;
    param.lF = 0.1; // measured
    param.lR = 0.107; // measured
    param.MaxWheelAngle = 18 * PI / 180.0; // From initial optimization
    param.MaxWheelSpeed = 352; // measured
    param.BackEMFGain_up = 9.0; // estimated using step data from none-loaded wheels
    param.TorqueMin = 0.12; // estimated using step data from none-loaded wheels
    param.BackEMFGain_down = 1.4; // estimated using step data from none-loaded wheels
    param.m = 1.143; // measured
    param.Iz = 1.0 / 12.0 * param.m * (0.1 * 0.1 + 0.2 * 0.2); // estimated by bar of width 10cm,
                                                               // length 20cm, mass 1143 g
    param.IwF = 0.8 * 3.2e-5; // approximate //wheel weight = 0.044 kg
    param.IwR = param.IwF;
    param.h = 0.02794; // roughly measured

    // Set default initial state
    state.x = state.y = state.Vx = state.Vy = 0.0;
    state.psi = state.dpsi = state.omegaF = state.omegaR = 0.0;

    delta = T_F = T_R = beta = 0.0;
    V_Fx = V_Fy = V_Rx = V_Ry = 0.0;
    mu_Fx = mu_Fy = mu_Rx = mu_Ry = 0.0;
    f_Fx = f_Fy = f_Rx = f_Ry = 0.0;
    omegaDes = 0.0;

    // construct a trivial random generator engine from a time-based seed:
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    generator.seed(seed);

    // Set up noise values
    noise.velocity_variance = 0.5; //4; //8;
    noise.dpsi_variance = 0.2; //1; //12;
    noise.omega_variance = 100;
    updateNoise();
}

CarDynamics::~CarDynamics()
{
    // TODO Auto-generated destructor stub
}

/**
 *
 * @return
 */
struct sCarState CarDynamics::getState(void)
{
    return this->state;
}

/**
 * Update the internal noise generators with the correct variances
 */
void CarDynamics::updateNoise(void)
{
    velocity_noise = std::normal_distribution<double>(0.0,
            noise.velocity_variance);
    dpsi_noise = std::normal_distribution<double>(0.0, noise.dpsi_variance);
    omega_noise = std::normal_distribution<double>(0.0, noise.omega_variance);
}

/**
 *
 * @return
 */
struct sCarParam CarDynamics::getParams(void)
{
    return this->param;
}

/**
 *
 * @return
 */
struct sNoiseParam CarDynamics::getNoise(void)
{
    return this->noise;
}

/**
 * Initialize state and reset time to zero
 * @param initState Initial state value
 */
void CarDynamics::setInitialState(struct sCarState initState)
{
    state = initState;
    simTime = 0;
}

/**
 * Set noise parameters for simulation
 * @param n
 */
void CarDynamics::setNoiseStruct(struct sNoiseParam n)
{
    noise = n;
    updateNoise();
}

/**
 * Set the magic formula coefficients
 * @param b
 * @param c
 * @param d
 */
void CarDynamics::setMFParams(double b, double c, double d)
{
    param.B = b;
    param.C = c;
    param.D = d;
}

/**
 * Set car parameters
 * @param p
 */
void CarDynamics::setParamStruct(struct sCarParam p)
{
    param = p;
}

/**
 * Choose which input variable you want to send, wheel speed or torque
 * @param cmdWheelSpeed True if you want to command wheel speed (default), false
 * otherwise
 */
void CarDynamics::chooseInputType(bool cmdWheelSpeed)
{
    commandWheelSpeed = cmdWheelSpeed;
}

/**
 * Convert normalized throttle and turn commands into torque and delta commands.
 * Sets internal torque and delta variables.
 * @param throttle Throttle input (-1 to 1)
 * @param turn Steering input (-1 to 1)
 */
void CarDynamics::setTorqueDelta(double throttle, double turn)
{
    throttle = acl::saturate(throttle, 1.0, -1.0);
    turn = acl::saturate(turn, 1.0, -1.0);

    if (not commandWheelSpeed)
    {
        T_R = throttle; // these could be more elaborate mapping functions
        T_F = throttle;
        delta = -turn * param.MaxWheelAngle; // delta is positive to the left
    }
    else
    {
        std::cout << "Error: should be setting desired wheel speed, not torque."
                << std::endl;
    }
}

/**
 * Sets internal desired wheel speed and delta variables.
 * @param omegaDes Desired wheel speed in rad/sec (-500 to 500)
 * @param turn Steering input (-1 to 1)
 */
void CarDynamics::setOmegaDelta(double omegaDes, double turn)
{
    omegaDes = acl::saturate(omegaDes, param.MaxWheelSpeed,
            -param.MaxWheelSpeed);
    turn = acl::saturate(turn, 1.0, -1.0);

    if (commandWheelSpeed)
    {
        this->omegaDes = omegaDes;
        delta = -turn * param.MaxWheelAngle; // delta is positive to the left
    }
    else
    {
        std::cout << "Error: should be setting torque, not desired wheel speed."
                << std::endl;
    }
}

/**
 * Compute the vehicle velocity components at the front and rear wheels
 */
void CarDynamics::calcVehicleComponetVelocities(void)
{
    beta = atan2(state.Vy, state.Vx);
    double V = acl::norm(state.Vx, state.Vy);
    if (fabs(V) < 0.01)
    {
        beta = 0.0;
        V = 0.0;
    }

    V_Fx = V * cos(beta - delta) + state.dpsi * param.lF * sin(delta);
    V_Fy = V * sin(beta - delta) + state.dpsi * param.lF * cos(delta);
    V_Rx = V * cos(beta);
    V_Ry = V * sin(beta) - state.dpsi * param.lR;
}

/**
 * Pacejka's "magic formula" for calculating tire friction
 * @param s Theoretical slip
 * @return Total friction coefficient
 */
double CarDynamics::MF(double s)
{
    return param.D * sin(param.C * atan(param.B * s));
}

/**
 * Calculate slip in x direction
 * @param Vx Body x velocity
 * @param omega Wheel speed
 * @return
 */
double CarDynamics::xSlip(double Vx, double omega, double r)
{
    if (omega * r != 0.0)
        return (Vx - omega * r) / fabs(omega * r);
    else
        return 0.0;
}

/**
 * Calculate slip in y direction
 * @param Vy Body y velocity
 * @param omega Wheel speed
 * @return
 */
double CarDynamics::ySlip(double Vy, double omega, double r)
{
    if (omega * r != 0.0)
        return Vy / fabs(omega * r);
    else
        return 0.0; // 1.0;
}

/**
 * Calculate friction coefficients for the front and rear wheels
 */
void CarDynamics::calcFrictionCoeffs(void)
{
    double s_Fx = xSlip(V_Fx, state.omegaF, param.rF);
    double s_Fy = ySlip(V_Fy, state.omegaF, param.rF);
    double s_F = acl::norm(s_Fx, s_Fy);

    double s_Rx = xSlip(V_Rx, state.omegaR, param.rR);
    double s_Ry = ySlip(V_Ry, state.omegaR, param.rR);
    double s_R = acl::norm(s_Rx, s_Ry);

    if (fabs(s_F) < TOL)
    {
        mu_Fx = mu_Fy = 0.0;
    }
    else
    {
        mu_Fx = -s_Fx / s_F * MF(s_F);
        mu_Fy = -s_Fy / s_F * MF(s_F);
    }

    if (fabs(s_R) < TOL)
    {
        mu_Rx = mu_Ry = 0.0;
    }
    else
    {
        mu_Rx = -s_Rx / s_R * MF(s_R);
        mu_Ry = -s_Ry / s_R * MF(s_R);
    }
}

/**
 * calculate the front and rear tire friction forces
 */
void CarDynamics::calcFrictionForces(void)
{
    double num = param.lR * param.m * GRAVITY
            - param.h * param.m * GRAVITY * mu_Rx;
    double den = param.h * (mu_Fx * cos(delta) - mu_Fy * sin(delta) - mu_Rx);
    double f_Fz = num / (param.lF + param.lR + den);
    double f_Rz = param.m * GRAVITY - f_Fz;
    f_Fx = mu_Fx * f_Fz;
    f_Fy = mu_Fy * f_Fz;
    f_Rx = mu_Rx * f_Rz;
    f_Ry = mu_Ry * f_Rz;
}

/**
 * Integrate the system dynamics one time step
 * @param dt Time step
 */
void CarDynamics::integrateStep(double dt)
{
    std::valarray<double> currentState(STATE_LENGTH);
    std::valarray<double> nextState(STATE_LENGTH);

    currentState[0] = state.x;
    currentState[1] = state.y;
    currentState[2] = state.Vx;
    currentState[3] = state.Vy;
    currentState[4] = state.psi;
    currentState[5] = state.dpsi;
    currentState[6] = state.omegaF;
    currentState[7] = state.omegaR;

//	if (fabs(state.omegaF) < 10.0)
//    	state.omegaF = 0.0;
//    if (fabs(state.omegaR) < 10.0)
//    	state.omegaR = 0.0;

    // run the RK4 integration step
    nextState = acl::rk4(simTime, currentState, dt,
            boost::bind(&CarDynamics::dynamics, this, _1, _2));
    simTime += dt;

    state.x = nextState[0];
    state.y = nextState[1];
    state.Vx = nextState[2];
    state.Vy = nextState[3];
    state.psi = nextState[4];
    state.dpsi = nextState[5];
    state.omegaF = acl::saturate(nextState[6], param.MaxWheelSpeed,
            -param.MaxWheelSpeed);
    state.omegaR = acl::saturate(nextState[7], param.MaxWheelSpeed,
            -param.MaxWheelSpeed);

}

double CarDynamics::wheelSpeed(double throttle, double omega, double f)
{

    double K = 0;

    if (throttle > 0.55)
        K = 75 * throttle + 290;
    else
        K = 3.79e3 * pow(throttle, 3) - 5.41e3 * pow(throttle, 2)
                + 2.66e3 * throttle - 1.29e2;

    if (throttle < param.TorqueMin)
        K = 0;
    else
        K = K / throttle;

    double dw = K * param.BackEMFGain_up * throttle
            - param.BackEMFGain_up * omega - 1 / param.IwR * f * param.rR;

    if (omega > param.MaxWheelSpeed && dw > 0)
        dw = 0;

    if (dw < 0)
        dw = K * param.BackEMFGain_down * throttle
                - param.BackEMFGain_down * omega - 1 / param.IwR * f * param.rR;

    return dw;
}

/**
 * System dynamics
 * @param dt Time step
 * @param s State
 * @return Derivative of state
 */
std::valarray<double> CarDynamics::dynamics(double dt, std::valarray<double> s)
{

    // double x  = s[0];
    // double y  = s[1];
    double Vx = s[2];
    double Vy = s[3];
    double psi = s[4];
    double dpsi = s[5];
    double wF = s[6];
    double wR = s[7];

    // get tire forces
    calcVehicleComponetVelocities();
    calcFrictionCoeffs();
    calcFrictionForces();

    // System dynamics
    double xdot = Vx * cos(psi) - Vy * sin(psi); // inertial x
    double ydot = Vx * sin(psi) + Vy * cos(psi); // inertial y
    double Vxdot = 1 / param.m
            * (param.m * Vy * dpsi + f_Fx * cos(delta) - f_Fy * sin(delta)
                    + f_Rx) + velocity_noise(generator); // body x velocity
    double Vydot = 1 / param.m
            * (-param.m * Vx * dpsi + f_Fx * sin(delta) + f_Fy * cos(delta)
                    + f_Ry) + velocity_noise(generator); // body y velocity
    double psidot = dpsi; // yaw
    double dpsidot = 1 / param.Iz
            * ((f_Fy * cos(delta) + f_Fx * sin(delta)) * param.lF
                    - f_Ry * param.lR) + dpsi_noise(generator); // yaw rate
    double wFdot, wRdot;
    if (commandWheelSpeed)
    {
        if (omegaDes > wF)
            wFdot = param.omegaCLup_a * omegaDes - param.omegaCLup_b * wF;
        else
            wFdot = param.omegaCLdown_a * omegaDes - param.omegaCLdown_b * wF;
        if (omegaDes > wR)
            wRdot = param.omegaCLup_a * omegaDes - param.omegaCLup_b * wR;
        else
            wRdot = param.omegaCLdown_a * omegaDes - param.omegaCLdown_b * wR;

        wFdot -= 300.1 * fabs(delta);
        wRdot -= 300.1 * fabs(delta);

    }
    else
    {

        // enforce that wF and wR are the same (like physical drift car)
        wFdot = wheelSpeed(T_F, wF, (f_Fx + f_Rx) / 2.0);
        wRdot = wFdot;

    }

    wRdot += omega_noise(generator);
    wFdot += omega_noise(generator);

    std::valarray<double> out(STATE_LENGTH);
    out[0] = xdot;
    out[1] = ydot;
    out[2] = Vxdot;
    out[3] = Vydot;
    out[4] = psidot;
    out[5] = dpsidot;
    out[6] = wFdot;
    out[7] = wRdot;

    return out;
}

} /* namespace acl */
