/*!
 * \file CartPoleDynamics.hpp
 *
 * Simple cart-pole system.  Equations of motion from
 * Barto, Sutton, and Anderson, "Neuronlike Adaptive Elements That Can Solve
 * Difficult Learning Control Problems," IEEE Trans. Syst., Man, Cybern.,
 * Vol. SMC-13, pp. 834--846, Sept.--Oct. 1983
 *
 *  Created on: May 28, 2013
 *      Author: Mark Cutler
 *	   Contact: markjcutler@gmail.com
 *
 */

#ifndef CARDYNAMICS_HPP_
#define CARDYNAMICS_HPP_

// Global includes
#include <valarray>
#include <boost/bind.hpp>

// Local includes
#include "../utils.hpp"

namespace acl
{

const unsigned int STATE_LENGTH = 4;

/// parameter struct
struct sCartPoleParam
{
    double m; ///< Pole mass (kg)
    double l; ///< Pole length (m)
    double mc; ///< Cart mass (kg)
    double muc; ///< Coefficient of friction of cart on track
    double mup; ///< Coefficient of friction of pole on cart
};

/// state struct
struct sCartPoleState
{
    double x; ///< Cart position (m)
    double dx; ///< Cart velocity (m/s)
    double theta; ///< Pole angle (rad)
    double dtheta; ///< Pole angular rate (rad/s)
};

/**
 *  CartPole dynamics
 */
class CartPoleDynamics
{
public:
    CartPoleDynamics();
    virtual ~CartPoleDynamics();

    void setForce(double F);
    double getForce(void);
    void setInitialState(struct sCartPoleState initState);

    struct sCartPoleState getState(void);
    struct sCartPoleParam getParams(void);

    void integrateStep(double dt);

private:
    std::string name; ///< cart name
    struct sCartPoleState state; ///< state
    struct sCartPoleParam param; ///< param struct

    double force; ///< Cart force (N)
    double simTime; ///< simulation time

    std::valarray<double> dynamics(double dt, std::valarray<double> s);
};

} /* namespace acl */
#endif /* CARDYNAMICS_HPP_ */
