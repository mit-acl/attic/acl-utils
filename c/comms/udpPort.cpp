/*!
 * \file udpPort.cpp
 *
 * Opens, reads from, and writes to a UDP port
 *
 *  Created on: Mar 15, 2013
 *      Author: Buddy Michini
 *
 */

#include "udpPort.hpp"

namespace acl
{

UDPPort::~UDPPort()
{
    udpClose();
}

int UDPPort::udpInit(bool bCast, char* addr, int RxPort, int TxPort)
{

    multicast = false;
    broadcast = bCast;

    // Receive port
    socketRx = socket(PF_INET, SOCK_DGRAM, 0);

    // Set 100ms timeout
    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 100000;
    if (setsockopt(socketRx, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0)
    {
        perror("Error");
    }

    bzero(&addressRx, sizeof(addressRx));

    addressRx.sin_family = AF_INET;
    addressRx.sin_port = htons(RxPort);
    addressRx.sin_addr.s_addr = INADDR_ANY;

    if (bind(socketRx, (struct sockaddr*) &addressRx, sizeof(addressRx)) != 0)
    {
        printf("\tUDP Error :: Couldn't bind to Rx port %d\n", RxPort);
        return 0;
    }

    printf("\tUDP listening on %s, port %d\n", addr, RxPort);

    // Broadcast port
    if (broadcast)
    {
        socketTx = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
        bzero(&addressTx, sizeof(addressTx));

        if (socketTx < 0)
            printf("\tUDP Error :: Couldn't create socket\n");

        const int on = 1;
        if (setsockopt(socketTx, SOL_SOCKET, SO_BROADCAST, &on, sizeof(on)) < 0)
            printf("\tUDP Error :: Couldn't set SO_BROADCAST\n");

        addressTx.sin_family = AF_INET;
        addressTx.sin_port = htons(TxPort);

        inet_aton(addr, &addressTx.sin_addr);
        printf("\tUDP broadcasting on %s, port %d\n", addr, TxPort);
    }

    return (1);
}

int UDPPort::udpInit(char* theLocalIP, char* theMultiIP, int inPort)
{

    multicast = true;

    strcpy(localIP, theLocalIP);
    strcpy(multiIP, theMultiIP);

    // Setup multicast receive port
    socketMc = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (socketMc < 0)
    {
        printf("\tUDP Error :: Couldn't create socket\n");
        return (-1);
    }

    bzero(&addressMc, sizeof(addressMc));
    addressMc.sin_family = AF_INET;
    addressMc.sin_port = htons(inPort);
    addressMc.sin_addr.s_addr = INADDR_ANY;

    if (bind(socketMc, (struct sockaddr*) &addressMc, sizeof(addressMc)) != 0)
    {
        printf("\tUDP Error :: Couldn't bind to port %d\n", inPort);
    }

    // Construct a multicast join request
    mc_req.imr_multiaddr.s_addr = inet_addr(multiIP);
    mc_req.imr_interface.s_addr = inet_addr(localIP);

    /* ADD MEMBERSHIP message via setsockopt */
    if ((setsockopt(socketMc, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char*) &mc_req,
            sizeof(mc_req))) < 0)
    {
        printf("\tAdd to multicast failed\n");
    }

    int reuse = 1;
    if ((setsockopt(socketMc, SOL_SOCKET, SO_REUSEADDR, (char*) &reuse,
            sizeof(int))) < 0)
    {
        printf("\tCouldn't set reuse \n");
    }

    inet_aton(multiIP, &addressMc.sin_addr);

    printf("\tMulticast UDP :: localIP %s, multiIP %s, port %d\n", localIP,
            multiIP, inPort);

    return (1);
}

int UDPPort::udpSend(unsigned char* pkt, unsigned int len)
{

    int chars_sent;
    if (multicast)
        chars_sent = sendto(socketMc, pkt, len, 0,
                (struct sockaddr*) &addressMc, sizeof(addressMc));
    else
        chars_sent = sendto(socketTx, pkt, len, 0,
                (struct sockaddr*) &addressTx, sizeof(addressTx));

    return chars_sent;
}

int UDPPort::udpReceive(char* buffer, int len)
{

    int addr_len;
    int bytes;

    if (multicast)
    {
        addr_len = sizeof(addressMc);
        bytes = recvfrom(socketMc, buffer, len, 0,
                (struct sockaddr*) &addressMc, (socklen_t*) &addr_len);
    }
    else
    {
        addr_len = sizeof(addressRx);
        bytes = recvfrom(socketRx, buffer, len, 0,
                (struct sockaddr*) &addressRx, (socklen_t*) &addr_len);
    }

    // printf("Buf = <%s>\n",buffer);
    return bytes;
}

void UDPPort::udpClose()
{

    if (multicast)
    {

        close(socketMc);

    }
    else
    {

        close(socketRx);

        if (broadcast)
            close(socketTx);
    }
}

} // end namespace acl
;
