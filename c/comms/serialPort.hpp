/*!
 * \file serialPort.hpp
 *
 * Opens, reads from, and writes to a serial port
 *
 *  Created on: Mar 15, 2013
 *      Author: Buddy Michini
 *
 */

#ifndef SERIALPORT_H_
#define SERIALPORT_H_

#include <stdio.h>
#include <iostream>
#include <string>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>

#include <sys/select.h>
#include <semaphore.h>
#include <signal.h>
#include <math.h>

/// Maximum serial port buffer size
#define SER_BUF_SZ 5000

namespace acl
{
class SerialPort
{

    int serial;
    char buffer[SER_BUF_SZ];

public:
    SerialPort();

    bool initialized;
    int spInitialize(std::string usbport, int baudrate, bool block);
    int spSend(char* pkt);
    int spSend(char* pkt, int sz);
    int spSend(const char* pkt, int sz);
    int spSend(uint8_t* pkt, uint8_t sz);
    char* spReceive();
    char* spReceive(int nbytes);
    char spReceiveSingle();
    int Receive( unsigned char * buf, int nBytes);
    int ReceiveSingle( unsigned char * buf);
    void spClose();
};
}
;

#endif
