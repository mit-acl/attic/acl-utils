/*!
 * \file structs.hpp
 *
 * Simple structs containing messages and integrators
 *
 *  Created on: Mar 15, 2013
 *      Author: Buddy Michini
 *
 */

#ifndef STRUCTS_HPP_
#define STRUCTS_HPP_

#include <math.h>

namespace acl
{
// MESSAGE ############
struct Message
{
    int src;
    int dest;
    int cmdID;
    double data[12];

    // Constructors:
    Message()
    {
        src = 0;
        dest = 0;
        cmdID = 0;
        for (int ii = 0; ii < 12; ii++)
        {
            data[ii] = 0.0;
        }
    }
};

// INTEGRATOR ############
struct Integrator
{
    double value;
    double AW_thresh;
    double AW_value;

    // Constructors:
    Integrator()
    {
        value = 0;
        AW_thresh = 1.0; // aw_thresh;
        AW_value = 0;
    }
    ;
    Integrator(double aw_thresh)
    {
        value = 0;
        AW_thresh = aw_thresh;
        AW_value = 0;
    }
    ;

    // Methods
    void increment(double inc, double dt)
    {
        if (fabs(value) < AW_thresh)
        {
            value += inc * dt;
        }
    }
    void reset()
    {
        value = 0;
        AW_value = 0;
    }
};

} // end namespace acl
;

#endif /* STRUCTS_HPP_ */
