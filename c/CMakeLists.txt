cmake_minimum_required(VERSION 2.8.12 FATAL_ERROR)
project(acl-utils)

# make sure you are building in release mode
set(CMAKE_BUILD_TYPE Release)

# need to latest c++ compiler standard
add_compile_options(-std=c++0x)

# Point Cloud Library needed for the pure puruit algorithm
find_package(PCL 1.2 REQUIRED)
include_directories(${PCL_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})

# build the library and link it to the PCL libraries
add_library(acl_utils SHARED 
            comms/BTPort.cpp
            comms/serialPort.cpp
            comms/udpPort.cpp
            comms/wifly.cpp
            dynamics/CarDynamics.cpp
            dynamics/CartPoleDynamics.cpp
            dynamics/InvertedPendulumDynamics.cpp
            dynamics/QuadDynamics.cpp
            dynamics/UpuccDynamics.cpp
            Quaternion.cpp
            timer.cpp
            utils.cpp
            control/pure_pursuit.cpp)
target_link_libraries(acl_utils 
    ${PCL_LIBRARIES} 
    bluetooth 
    boost_filesystem 
    boost_program_options 
    boost_system 
    boost_thread)

