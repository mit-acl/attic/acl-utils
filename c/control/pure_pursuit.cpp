/*!
 * \file pure_pursuit.cpp
 *
 * See pure_pusuit.h for description.
 *
 * Created on: Jan 22, 2014
 * 	   Author: Mark Cutler
 *      Email: markjcutler@gmail.com
 */

#include "pure_pursuit.h"

namespace acl
{

/**
 * Pure pursuit constructor
 * @param path n by 4 vector containing discretized path (x,y,z) and associated
 * 			speed commands
 * @param L1_nom Nominal L1 lookahead point in meters
 * @param L1_gain Gain by which L1 lookahead point will be scaled based on
 * commanded velocity
 */
PurePursuit::PurePursuit(std::vector<std::vector<double> > path, double L1_nom,
        double L1_gain)
{
    // Set private variables
    this->L1_nom = L1_nom;
    this->L1 = L1_nom;
    this->L1_gain = L1_gain;
    this->vcmd_min = L1_nom / L1_gain;

    // Build a kdtree of the path data
    updateTree(path);
}

PurePursuit::~PurePursuit()
{
    // TODO Auto-generated destructor stub
}

/**
 * Update kdtree data -- marginally faster than just defining a new instance
 * of the PurePursuit class
 * - Note: there is no good way to append data to a kdtree, so the only way
 *         to add data to it is to rebuild the entire tree
 * @param n by 4 vector containing discretized path (x,y,z) and associated
 * 			speed commands
 */
void PurePursuit::updateTree(std::vector<std::vector<double> > path_in)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr path_tmp(
            new pcl::PointCloud<pcl::PointXYZ>);
    path = path_tmp;

    // Generate pointcloud data
    path->width = path_in.size();
    path->height = 1;
    path->points.resize(path->width * path->height);
    speed.resize(path_in.size());

    for (size_t i = 0; i < path->points.size(); ++i)
    {
        path->points[i].x = path_in[i][0];
        path->points[i].y = path_in[i][1];
        path->points[i].z = path_in[i][2];
        speed[i] = path_in[i][3];
    }

    kdtree.setInputCloud(path);
}

/**
 * Main function for computing the L1 control parameters
 * @param p Vector containing current x,y,z positions
 * @param v Vector containing current dx,dy,dz velocities
 * @param psi Current heading angle in radians
 * @param pL1 L1 control point
 * @param v_cmd Commanded velocity (speed really)
 * @param r_cmd Commanded angular rate
 * @param psi_cmd Commanded heading angle
 * @param index_close Index of the closest point on the path
 * @param index_L1 Index of the L1 lookahead point on the path
 * @return 1 if successfully calculated desired control, 0 otherwise
 */
int PurePursuit::calcL1Control(std::vector<double> p, std::vector<double> v,
        double psi, std::vector<double>& pL1, double& v_cmd, double& r_cmd,
        double& psi_cmd, int& index_close, int& index_L1)
{
    // Find the L1 point
    if (not findL1(p, index_close, index_L1))
        return 0;

    // L1 control (in the vehicle body frame
    pcl::PointXYZ pL1_tmp;
    pL1_tmp = path->points[index_L1];
    pL1[0] = pL1_tmp.x;
    pL1[1] = pL1_tmp.y;
    pL1[2] = pL1_tmp.z;
    double eta = findEta(p, v, pL1_tmp);
    v_cmd = speed[index_close]; // speed command comes from closest
    // point, NOT the L1 lookahead point

    double vnorm = acl::norm(v[0], v[1], v[2]);
    vnorm = acl::saturate(vnorm, 4.0, 0.0); // TODO: make this an input, not hard coded
    double a_cmd = 2.0 * vnorm * vnorm / L1 * sin(eta);
    psi_cmd = acl::wrap(eta + psi);

    // Path angular rate command
    // a_cmd = v^2/R = R*omega^2
    // omega = sqrt(a_cmd*2*sin(eta)/L1)
    r_cmd = sqrt(a_cmd * 2.0 * sin(eta) / L1);
    if (eta < 0.0)
        r_cmd *= -1.0;

    // Update L1 based on magnitude of the velocity
    if (vnorm > vcmd_min)
        L1 = L1_gain * vnorm;
    else
        L1 = L1_nom;

    return 1;
}

/**
 * Find eta, the angle between L1 look ahead point and the current velocity
 * vector
 * @param p
 * @param v
 * @param pL1
 * @return
 */
double PurePursuit::findEta(std::vector<double> p, std::vector<double> v,
        pcl::PointXYZ pL1)
{
    double theta_V = atan2(v[1], v[0]);
    std::vector<double> L1 = p;
    L1[0] = pL1.x - p[0];
    L1[1] = pL1.y - p[1];
    double theta_L1 = atan2(L1[1], L1[0]);
    double eta = theta_L1 - theta_V;
    return acl::wrap(eta);
}

int PurePursuit::findL1(std::vector<double> p, int& index_closest,
        int& L1_index)
{
    pcl::PointXYZ searchPoint;
    searchPoint.x = p[0];
    searchPoint.y = p[1];
    searchPoint.z = p[2];

    // find the nearest neighbor to the search point
    std::vector<int> indNKS(1);
    std::vector<float> dist2NKS(1);

    if (kdtree.nearestKSearch(searchPoint, 1, indNKS, dist2NKS) <= 0)
        return 0;

    index_closest = indNKS[0];
    L1_index = indNKS[0];

    // If the closest point is more than L1 away, just return this point
    if (dist2NKS[0] >= L1)
        return 1;

    // Otherwise, search for all the nearest points within a distance L1 of
    // the current point
    std::vector<int> indRS;
    std::vector<float> dist2RS;

    if (kdtree.radiusSearch(searchPoint, L1, indRS, dist2RS) > 0)
    {
        // Because the path data is assumed sequential, the largest index will
        // be the point on the path that is L1 away from you in the 'forward'
        // direction
        L1_index = *std::max_element(indRS.begin(), indRS.end());
    }

    return 1;
}
} /* end namespace acl */
